﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace WMSAPITestHardness
{
    [JsonObject("customerOrder")]
    public class CustomerOrder
    {
        [JsonProperty("orderNumber")]
        public string OrderNumber { get; set; }
        [JsonProperty("orderDate")]
        public string OrderDate { get; set; }
        [JsonProperty("orderStatus")]
        public string OrderStatus { get; set; }
    }

    
}
