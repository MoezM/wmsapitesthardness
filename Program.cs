﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;

namespace WMSAPITestHardness
{
    class Program
    {
      
        const string endPoint = "https://c59af63a-58e7-4a4b-82ef-50872fef55fe.mock.pstmn.io";
        const string getOrdersMethod = "/order?customerNo={0}&username=u&password=p";
        const string getOrdersNoAuth = "/order?customerNo={0}";
        const string badRequestUrl = "/order";
        const int responseTimeBenchMark = 10000; //Millisecond  

        //Note: Presumed the selected customer 12345 has 3 orders in WMS, 
        //change customerId and count with a known customer history
        //This is to validate if API is returning accurate order count 
        const string customerId = "12345";
        const int orderHistoryCount = 3;

        static void Main(string[] args)
        {
          
        
            string url = GetAPIUrl(customerId);
            string urlNoAuth = endPoint + String.Format(getOrdersNoAuth, customerId);
            API_Should_Respond_Under_Response_Time_Benchmark();
            API_Should_Reject_Anonymous_Call_And_Return_401(urlNoAuth);
            API_Should_Return_404_For_Not_Found_Path(endPoint+ badRequestUrl);
            API_Should_Return_All_Past_Orders();
            API_Should_Return_Valid_DateFormat();
            API_Should_Return_Valid_Json_Array(url, customerId);


        }

        public static void API_Should_Respond_Under_Response_Time_Benchmark()
        {
            try
            {
                Stopwatch watch = new Stopwatch();
                watch.Start();
                var OrdersForTestCustomer = GetOrders(customerId);
                watch.Stop();
                Console.WriteLine("Passed: API response time benchmark test Passed. Elapsed Time is " + watch.ElapsedMilliseconds);

                if (watch.ElapsedMilliseconds > responseTimeBenchMark)
                    throw new TimeoutException("API response time exceeded benchmark");
                            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed: API Response time Failed - " + ex.Message);

            }
        }

        public static void API_Should_Return_404_For_Not_Found_Path(string url) //404
        {
            try
            {
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(url);

                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                if (myHttpWebResponse.StatusCode == HttpStatusCode.OK)
                    Console.WriteLine("Failed: Request Not Found Failed!", myHttpWebResponse.StatusDescription);
                myHttpWebResponse.Close();

            }
            catch (WebException ex)
            {
                var response = (HttpWebResponse)ex.Response;
                if (response.StatusCode == HttpStatusCode.NotFound)
                Console.WriteLine("Passed: Request Not Found response for invalid path Passed: " + ex.Message);
                else
                    Console.WriteLine("Failed: Request Not Found did not returned 404 " + ex.Message);
            }
        }

        public static void API_Should_Reject_Anonymous_Call_And_Return_401(string uri)
        {
            try
            {
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(uri);

                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                if (myHttpWebResponse.StatusCode == HttpStatusCode.OK)
                    Console.WriteLine("Failed: Anonymous Call rejection Failed!", myHttpWebResponse.StatusDescription);
                myHttpWebResponse.Close();

            }
            catch (WebException ex)
            {
                var response = (HttpWebResponse)ex.Response;
                if (response.StatusCode == HttpStatusCode.Unauthorized)  //401 
                    Console.WriteLine("Passed: Anonymous Call rejection Passed with API Response" + ex.Message);
                else
                    Console.WriteLine("Failed: Anonymous rejected but did not returned 401:" + ex.Message);
            }
        }

        

        public static void API_Should_Return_All_Past_Orders()
        {
            try
            {
                var OrdersForTestCustomer = GetOrders(customerId);
                Assert.AreEqual(OrdersForTestCustomer.Count, orderHistoryCount);
                Console.WriteLine("Passed: Order History count validated OK");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed: Invalid Order History count" + ex.Message);

            }
        }

        public static void API_Should_Return_Valid_DateFormat()
        {
            try
            {
                var OrdersForTestCustomer = GetOrders(customerId);

                DateTime orderDate;
                if (DateTime.TryParse(OrdersForTestCustomer[0].OrderDate, out orderDate))
                {
                    Console.WriteLine("Passed: Date Format validated OK");
                }
                else
                {
                    Console.WriteLine("Failed: Date Format is Not valid.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed: Date format validation error:" + ex.Message);
            }
        }


        public static void API_Should_Return_Valid_Json_Array(string uri, string customerId)
        {
            try
            {
                    var orders = GetOrders(customerId);
                Console.WriteLine("Passed: Responce format validation test Passed.");
                Console.WriteLine("\n");
                Console.WriteLine("Test CustomerId:" + customerId);
                    Console.WriteLine("\n");
                    Console.WriteLine("------------------------\n");
                    foreach (var o in orders)
                    {
                        Console.WriteLine("Order Number: "+ o.OrderNumber + "\n");
                        Console.WriteLine("Date:" + o.OrderDate + "\n");
                        Console.WriteLine("Status" + o.OrderStatus + "\n");
                    }
                 
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed: JSON format is not valid" + ex.Message);
            }
        }

        public static List<CustomerOrder> GetOrders(string customerId)
        {
            string uri = endPoint + String.Format(getOrdersMethod, customerId);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                var jsonResponse = reader.ReadToEnd();
                var orders = JsonConvert.DeserializeObject<List<CustomerOrder>>(jsonResponse);
                return orders;
            }
        }
        public static string GetAPIUrl(string customerId)
        {
            return endPoint + String.Format(getOrdersMethod, customerId);
        }
    }
}




